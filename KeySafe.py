#!/usr/bin/env python

import cPickle
import argparse
import string
import random
import datetime
from fuzzywuzzy import fuzz
from subprocess import call
from os.path import isfile
from dateutil.relativedelta import relativedelta
from termcolor import colored
from selenium import webdriver
from selenium.common.exceptions import WebDriverException

__author__ = "Akshay R. Kapadia"
__copyright__ = "Copyright 2018, Akshay R. Kapadia"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Akshay R. Kapadia"
__email__ = "akshayrkapadia@tutamail.com"
__status__ = "Development"

keysafe = []
name = str


def load():
    global keysafe
    global name
    try:
        if isfile("KeySafeData.txt.gpg"):  # Checks to see if the file exists
            decrypt()
        with open(r"KeySafeData.txt", "rb") as data_file:
            keysafe_data = cPickle.load(data_file)
            keysafe = keysafe_data["KeySafe"]
            name = keysafe_data["Name"]
    except IOError:
        name = str(raw_input("Name Associated With GPG Key: "))


# Encrypts the "KeySafeData.txt" file to "KeySafeData.txt.gpg"
# Destroys the leftover file
def encrypt():
    call(["gpg", "-e", "-r", str(name), "KeySafeData.txt"])
    call(["shred", "-u", "KeySafeData.txt"])


# Decrypts the "KeySafeData.txt.gpg" file to "KeySafeData.txt"
# Destroys the leftover file
def decrypt():
    call(["gpg", "-d", "-o", "KeySafeData.txt", "KeySafeData.txt.gpg"])
    call(["shred", "-u", "KeySafeData.txt.gpg"])


# Stores the keysafe and name global variables in a dictionary
# Serializes the dictionary to "KeySafeData.txt"
# Encrypts the file
def save():
    if isfile("KeySafeData.txt.gpg"):
        decrypt()
    data = {"KeySafe": keysafe, "Name": name}
    with open(r"KeySafeData.txt", "wb") as data_file:
        cPickle.dump(data, data_file)
    encrypt()


# Today's date
def today():
    return datetime.date.today()


# The date in the future a certain number of months ahead (default is 6 months)
def future_date(starting_date, months_ahead):
    if months_ahead == 0:
        return "Never"
    return starting_date + relativedelta(months=+months_ahead)


# Prints all the account details
def show_account(account):
    print(colored(account["Name"], "red"))
    print("  Username: " + colored(account["Username"], "blue"))
    print("  Password: " + colored(account["Password"], "blue"))
    print("  URL: " + colored(account["URL"], "blue"))
    print("  Category: " + colored(account["Category"], "blue"))
    print("  Date Modified: " + colored(str(account["Date Modified"]), "blue"))
    print("  Password Expiration: " + colored(str(account["Password Expiration"]), "blue"))
    print("  Autologin: " + colored(str(account["Autologin"]), "blue"))


# Generates a random password using the ASCII character set with the specified length
def generate_password(length):
    while True:
        password = "".join(
            random.SystemRandom().choice(string.printable.strip(string.whitespace)) for _ in range(length))
        print("Password: " + colored(password, "blue"))
        password_option = str(raw_input("(r)egenerate or (c)ontinue: ")).lower()
        if password_option == "r":
            continue
        elif password_option == "c":
            return password


# Fuzzy search for matching accounts with the given name
def fuzzy_search(searched_name):
    matching_accounts = filter(lambda account: fuzz.partial_ratio(account["Name"], searched_name) > 75, keysafe)
    if len(matching_accounts) == 0:
        raise ValueError
    else:
        return matching_accounts


# Select an account from the fuzzy_search() result
def select_account(accounts):
    if len(accounts) == 1:
        return accounts[0]
    else:
        for account, i in zip(accounts, range(len(accounts))):
            print(str(i) + ") " + colored(account["Name"], "red") + "\n  Username: " + colored(account["Username"],
                                                                                               "blue") + "\n  URL: " + colored(
                account["URL"], "blue"))
        while True:
            try:
                account_index = int(raw_input("Select An Account Number: "))
                if account_index not in range(len(accounts)):
                    raise IndexError
                else:
                    return accounts[account_index]
            except (ValueError, IndexError):
                print(colored("Invalid Input, Try Again", "red"))


# Adds a new account to the keysafe
def add_account(account_name, username, password, category, url, months_ahead=6):
    date_modified = today()
    password_expiration = future_date(date_modified, months_ahead)
    account = {"Name": account_name, "Username": username, "Password": password, "Category": category, "URL": url,
               "Date Modified": date_modified, "Password Expiration": password_expiration, "Username ID": None,
               "Password ID": None, "Autologin": False}
    global keysafe
    keysafe.append(account)


# Deletes the account from the keysafe
def delete_account(account_name):
    matching_accounts = fuzzy_search(account_name)
    account = select_account(matching_accounts)
    global keysafe
    del keysafe[keysafe.index(account)]


# Edits the specified attribute for the specified account
def edit_account(account_name, attribute, new_value, months_ahead=None):
    try:
        matching_accounts = fuzzy_search(account_name)
        account = select_account(matching_accounts)
        account[attribute] = new_value
        account["Date Modified"] = today()
        if attribute == "Password":
            if months_ahead is not None:
                account["Password Expiration"] = future_date(account["Date Modified"], months_ahead)
            elif not account["Password Expiration"] == "Never":
                account["Password Expiration"] = future_date(account["Date Modified"], 6)
    except ValueError:
        print(colored("Account Does Not Exist", "red"))


# Logins into the website using the selenium webdriver
def login(account_name):
    try:
        matching_accounts = fuzzy_search(account_name)
        account = select_account(matching_accounts)
        if account["Autologin"]:
            browser = webdriver.Firefox() # Uses firefox browser
            browser.implicitly_wait(15)
            browser.maximize_window()
            browser.get(account["URL"])
            username_field = browser.find_element_by_id(account["Username ID"])
            username_field.clear()
            username_field.send_keys(account["Username"])
            password_field = browser.find_element_by_id(account["Password ID"])
            password_field.clear()
            password_field.send_keys(account["Password"])
            password_field.submit()
        else:
            raise IndexError
    except ValueError:
        print(colored("Account Does Not Exist", "red"))
    except IndexError:
        print(colored("Account Is Not Configured For AutoLogin", "red"))
    except WebDriverException:
        print(colored("Incorrect Username ID or Password ID", "red"))


def main():
    parser = argparse.ArgumentParser(prog="KeySafe", description="Secure Password Manager With GPG Encryption",
                                     epilog="KeySafe Copyright (C) 2018 Akshay R. Kapadia")
    subparsers = parser.add_subparsers(dest="command")  # Primary command
    a_subparser = subparsers.add_parser("add")
    d_subparser = subparsers.add_parser("delete")
    e_subparser = subparsers.add_parser("edit")
    f_subparser = subparsers.add_parser("find")
    c_subparser = subparsers.add_parser("config")
    l_subparser = subparsers.add_parser("login")
    s_subparser = subparsers.add_parser("see")

    # Add account parser
    a_subparser.add_argument("-x", "--password-expiration",
                             help="In how many months should the password expire (Enter 0 to make it last forever)",
                             type=int)
    a_subparser.add_argument("name", help="Name of the account", type=str)
    a_subparser.add_argument("username", help="Username for the account", type=str)
    a_subparser.add_argument("password", help="Password for the account (enter 'rng' to generate one)", type=str)
    a_subparser.add_argument("category", help="The category the account belongs to",
                             choices=["web", "social", "computer", "banking", "email", "other"], type=str)
    a_subparser.add_argument("url", help="The url for the website", type=str)

    # Delete account parser
    d_subparser.add_argument("name", help="Name of the account", type=str)

    # Edit account parser
    e_subparser.add_argument("-x", "--password-expiration",
                             help="In how many months should the password expire (Enter 0 to make it last forever)",
                             type=int)
    e_subparser.add_argument("name", help="Name of the account", type=str)
    e_subparser.add_argument("attribute", help="The attribute that needs to be changed",
                             choices=["name", "username", "password", "category", "url"], type=str)
    e_subparser.add_argument("value", help="The new value for the attribute", type=str)

    # Find account parser
    f_subparser.add_argument("name", help="Name of the account", type=str)

    # Configure autologin parser
    c_subparser.add_argument("name", help="Name of the account", type=str)
    c_subparser.add_argument("username_id", help="The html ID for the username field", type=str)
    c_subparser.add_argument("password_id", help="The html ID for the password field", type=str)

    # Autologin parser
    l_subparser.add_argument("name", help="Name of the account", type=str)

    # See category parser
    s_subparser.add_argument("category", help="The category that you want to see",
                             choices=["web", "social", "computer", "banking", "email", "other", "all"], type=str)

    args = vars(parser.parse_args())

    load()

    if args["command"] == "add":
        args["password"] = generate_password(int(raw_input("Password Length: "))) if (args["password"] == "rng") else \
            args["password"]
        args["category"] = (args["category"].lower()).capitalize()
        if args["password_expiration"] is not None:
            add_account(args["name"], args["username"], args["password"], args["category"], args["url"],
                        args["password_expiration"])
        else:
            add_account(args["name"], args["username"], args["password"], args["category"], args["url"])
    elif args["command"] == "delete":
        delete_account(args["name"])
    elif args["command"] == "find":
        try:
            matching_accounts = fuzzy_search(args["name"])
            for account in matching_accounts:
                show_account(account)
        except ValueError:
            print(colored("Account Does Not Exist", "red"))
    elif args["command"] == "edit":
        args["attribute"] = (args["attribute"].lower()).capitalize()
        while True:
            if args["attribute"] == "Password":
                args["value"] = generate_password(int(raw_input("Password Length: "))) if (args["value"] == "rng") else \
                    args["value"]
                if args["password_expiration"] is not None:
                    edit_account(args["name"], args["attribute"], args["value"], args["password_expiration"])
                    break
            elif args["attribute"] == "Category":
                if args["value"] not in ["web", "social", "computer", "banking", "email", "other"]:
                    print(colored("Invalid Category", "red"))
                    print(colored("Categories: Web, Social, Computer, Banking, Email, Other", "red"))
                else:
                    args["value"] = (args["value"].lower()).capitalize()
            edit_account(args["name"], args["attribute"], args["value"])
            break
    elif args["command"] == "config":
        try:
            matching_accounts = fuzzy_search(args["name"])
            account = select_account(matching_accounts)
            account["Username ID"] = args["username_id"]
            account["Password ID"] = args["password_id"]
            account["Autologin"] = True
        except ValueError:
            print(colored("Account Does Not Exist", "red"))
    elif args["command"] == "login":
        login(args["name"])
    elif args["command"] == "see":
        args["category"] = (args["category"].lower()).capitalize()
        if args["category"] == "All":
            for account in keysafe:
                show_account(account)
        else:
            for account in keysafe:
                if account["Category"] == args["category"]:
                    show_account(account)
    save()


if __name__ == "__main__":
    main()
